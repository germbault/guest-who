export interface ImageGame {
    id: number;
    fileName: string;
    done: boolean;
}
