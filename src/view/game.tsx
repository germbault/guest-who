import React from 'react';
import { images } from 'view/imageGallery';
import { ImageGame } from '../model/imagegame';

interface Props { }

interface State { className: string, images: ImageGame[]; }

export class Game extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            images: images,
            className: ''
        };
    }

    public render() {
        return (
            <section className='bloc-page'>
                <div>
                    <h2 className='content'>Game</h2>
                    <p><strong>1-</strong> Pour commencer veillez sélectionner votre image?</p>
                    <p><strong>2-</strong> Bon jeu.</p>
                    <div className='col-06' >
                        {this.state.images.map(image =>
                            <img className={this.state.className}
                                swapdone={() => this.swapDone(image)}
                                onClick={() => this.handleClick()}
                                key={image.id}
                                src={`img/${image.fileName}.jpg`}
                            />
                        )}
                    </div>
                    <div className='content'>
                        <button onClick={() => this.resetGame()}>Réniatialiser le jeu</button>
                    </div>
                </div>
            </section>
        );
    }

    private handleClick() {
        this.setState({
            className: (this.state.className === '') ? 'is-active' : ''
        });
    }

    private swapDone = (image: ImageGame) => {
        image.done = !image.done;
        this.setState({ images: this.state.images });
    };

    private resetGame = () => {
        this.setState({
            className: (!this.state.className) ? '' : ''
        });
    };
};
