import React from 'react';

export class Home extends React.Component<{}> {
    public render() {
        return (
            <section className='bloc-page'>
                <div>
                    <h2>Guess Who? Édition hockey</h2>
                    Le défi : deviner le personnage mystère de son adversaire avant qu'il ne devine le nôtre.
                    Pour les joueurs qui aiment les jeux à indices, Guess Who? classique est tout désigné!
                    On pose des questions par oui ou non à propos du personnage mystère et on élimine les personnages qui ne
                    correspondent pas à la description qui se précise dans notre tête. Quand on croit avoir deviné le
                    personnage mystère, on peut se prononcer, mais attention! On ne peut tenter de deviner qu'une fois par tour!
                    Le joueur qui devine le premier le personnage mystère de son adversaire gagne! Le jeu inclut 24 image de joueur de hockey. Voila un défi
                    à votre hauteur.
                </div>
            </section>
        );
    }
}
