import React from 'react';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import { Game } from 'view/game';
import { Home } from 'view/home';

export class Router extends React.Component<{}> {
    public render() {
        return <BrowserRouter>
            <header>
                <section className='bloc-page'>
                    <nav className='nav'>
                        <ul className='flex-grid'>
                            <li><Link to='/'>Home</Link></li>
                            <li><Link to='/game'>Game</Link></li>
                        </ul>
                    </nav>
                </section>
            </header>
            <Switch>
                <Route exact={true} path='/'><Home /></Route>
                <Route path='/game'><Game /></Route>
            </Switch>
        </BrowserRouter>;
    }
}
