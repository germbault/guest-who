import { Router } from 'data/router';
import React from 'react';
import ReactDom from 'react-dom';

ReactDom.render(
    <Router />,
    document.getElementById('coreContainer') // changer l’id dans le fichier index
);
